package dev.kkanit.controllers;

import dev.kkanit.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AuthControllerIntegrationTest {

    private static final JavalinApp app = new JavalinApp();
    private static final AuthController authController = new AuthController();
    private HttpResponse response;

    @BeforeAll
    public static void startServer() {app.start();}

    @AfterAll
    public static void stopServer() {app.stop();}

    @Test
    public void testLoginGet() {
        response = Unirest.get("http://localhost:7000/login").asString();
        assertAll(
                ()->assertEquals(401,response.getStatus()),
                ()->assertEquals("You must login to perform a get request on this resource.",response.getBody())
        );
    }

    @Test
    public void testLoginPostSuccess() {
        response = Unirest.post("http://localhost:7000/login").field("username", "pamelinaking").
                field("password","pamelinakingspassword").asString();
        assertAll(
                ()->assertEquals(200,response.getStatus()),
                ()->assertTrue(response.getBody().getClass()==String.class && ((String)response.getBody()).length() > 20)
        );

    }

    @Test
    public void testLoginPostFailure() {
        response = Unirest.post("http://localhost:7000/login").field("username", "pamelinaking").
                field("password","pamelinakingswrongpassword").asString();
        assertAll(
                ()->assertEquals(401,response.getStatus()),
                ()->assertEquals("Invalid login credentials submitted",response.getBody())
        );
    }

    @Test
    public void testLoginPostIncomplete() {
        response = Unirest.post("http://localhost:7000/login").asString();
        assertAll(
                ()->assertEquals(401,response.getStatus()),
                ()->assertEquals("You must enter a valid Username and Password",response.getBody())
        );
    }
}
