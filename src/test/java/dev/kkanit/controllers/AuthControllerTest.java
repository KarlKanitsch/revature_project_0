package dev.kkanit.controllers;

import dev.kkanit.services.PrivilegeService;
import dev.kkanit.services.UserService;
import io.javalin.http.UnauthorizedResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import io.javalin.http.Context;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class AuthControllerTest {

    @InjectMocks
    private AuthController authController;
    @Mock
    private UserService userService;
    private PrivilegeService privilegeService;
    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }

    private static Logger logger = LoggerFactory.getLogger(AuthControllerTest.class);
    private static Context context = mock(Context.class);

    @Test
    public void testAuthenticateUserHandler() {
        when(context.formParam("username")).thenReturn("username");
        when(context.formParam("password")).thenReturn("password");
        when(userService.authenticate("username","password")).thenReturn(1);
        when(userService.generateAuthToken("username", 1)).thenReturn("Fake JWT returned.");
        authController.handleAuthenticateUser(context);
        verify(context).json("Fake JWT returned.");
    }

    @Test
    public void testAuthenticateUserNullUsernamePassword() {
        when(context.formParam("username")).thenReturn(null);
        when(context.formParam("password")).thenReturn(null);
        assertThrows(UnauthorizedResponse.class, () -> {
            authController.handleAuthenticateUser(context);
        });
    }

    @Test
    public void testAuthenticateUserInvalidPassword() {
        when(context.formParam("username")).thenReturn("username");
        when(context.formParam("password")).thenReturn("wrongpassword");
        assertThrows(UnauthorizedResponse.class,()->authController.handleAuthenticateUser(context));
    }

    @Test
    public void testGetPrivilege() {
        Map hMap = new HashMap();
        hMap.put("auth-token","FakeAuthToken");
        hMap.put("privilege",1);
        when(context.headerMap()).thenReturn(hMap);
        when(userService.validateAuthToken("FakeAuthToken")).thenReturn(hMap);
        assertEquals(1,authController.getPrivilege(context));
    }
}
