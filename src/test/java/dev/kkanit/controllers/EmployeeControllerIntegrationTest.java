package dev.kkanit.controllers;

import dev.kkanit.JavalinApp;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class EmployeeControllerIntegrationTest {

    private static final JavalinApp app = new JavalinApp();
    private HttpResponse response;

    @BeforeAll
    public static void startServer() {app.start();}

    @AfterAll
    public static void stopServer() {app.stop();}

    @Test
    public void testHandlePutEmployeeRequest() {
        response = Unirest.post("http://localhost:7000/login").field("username", "kerstinfroment").
                field("password","kerstinfromentspassword").asString();
        String authToken = (String) response.getBody();
        response = Unirest.put("http://localhost:7000/employee")
                .field("first-name","New")
                .field("last-name","Employee")
                .field("pay-rate","25.50")
                .field("federal-id","123-45-6789")
                .header("auth-token", authToken)
                .asString();
        assertAll(
                ()->assertEquals(201,response.getStatus()),
                ()->assertTrue(((String) response.getBody()).contains("New employee successfully added."))
        );
    }

    @Test
    public void testHandlePutEmployeeRequestBadPayrate() {
        response = Unirest.post("http://localhost:7000/login").field("username", "kerstinfroment").
                field("password","kerstinfromentspassword").asString();
        String authToken = (String) response.getBody();
        response = Unirest.put("http://localhost:7000/employee")
                .field("first-name","New")
                .field("last-name","Employee")
                .field("pay-rate","20oz. gold")
                .field("federal-id","123-45-6789")
                .header("auth-token", authToken)
                .asString();
        assertAll(
                ()->assertEquals(500,response.getStatus()),
                ()->assertTrue(((String) response.getBody()).contains("There was a problem adding the new employee."))
        );
    }

    @Test
    public void testHandlePutEmployeeRequestUnAuthorized() {
        response = Unirest.post("http://localhost:7000/login").field("username", "dillonleneve").
                field("password","dillonlenevespassword").asString();
        String authToken = (String) response.getBody();
        response = Unirest.put("http://localhost:7000/employee")
                .field("first-name","New")
                .field("last-name","Employee")
                .field("pay-rate","25.50")
                .field("federal-id","123-45-6789")
                .header("auth-token", authToken)
                .asString();
        assertAll(
                ()->assertEquals(401,response.getStatus()),
                ()->assertTrue(((String) response.getBody()).contains("You do not have permission to hire new employees."))
        );
    }

//    @Disabled
    @Test
    public void testHandleDeleteEmployeeRequest() {
        response = Unirest.post("http://localhost:7000/login").field("username", "kerstinfroment").
                field("password","kerstinfromentspassword").asString();
        String authToken = (String) response.getBody();
        response = Unirest.delete("http://localhost:7000/employee")
                .field("employee-id","66")
                .header("auth-token", authToken)
                .asString();
        assertAll(
                ()->assertEquals(201,response.getStatus()),
                ()->assertTrue(((String) response.getBody()).contains("Employee successfully deleted from database."))
        );
    }

//    @Disabled
    @Test
    public void testHandleDeleteEmployeeRequestBadEmployeeID() {
        response = Unirest.post("http://localhost:7000/login").field("username", "kerstinfroment").
                field("password","kerstinfromentspassword").asString();
        String authToken = (String) response.getBody();
        response = Unirest.delete("http://localhost:7000/employee")
                .field("employee-id","blue")
                .header("auth-token", authToken)
                .asString();
        assertAll(
                ()->assertEquals(500,response.getStatus()),
                ()->assertTrue(((String) response.getBody()).contains("There was a problem deleting the employee."))
        );
    }

//    @Disabled
    @Test
    public void testHandleDeleteEmployeeRequestUnAuthorized() {
        response = Unirest.post("http://localhost:7000/login").field("username", "dillonleneve").
                field("password","dillonlenevespassword").asString();
        String authToken = (String) response.getBody();
        response = Unirest.delete("http://localhost:7000/employee")
                .field("employee-id","70")
                .header("auth-token", authToken)
                .asString();
        assertAll(
                ()->assertEquals(401,response.getStatus()),
                ()->assertTrue(((String) response.getBody()).contains("You do not have permission to delete employees."))
        );
    }
}
