package dev.kkanit.models;

//import java.text.MessageFormat;
//import java.util.Locale;
//import java.util.ResourceBundle;
//
//public class Localizer {
//
//    private final ResourceBundle bundle;
//
//    public Localizer(Locale locale) {
//        bundle = ResourceBundle.getBundle("localization", locale);
//    }
//
//    public String localize(String key) {
//        return bundle.getString(key);
//    }
//
//    public String localize(String key, Object... params) {
//        return MessageFormat.format(localize(key), params);
//    }
//}

import gg.jte.support.LocalizationSupport;
import java.util.*;

public class Localizer implements LocalizationSupport {

    private final ResourceBundle bundle;

    public Localizer(Locale locale) {
        bundle = ResourceBundle.getBundle("localization", locale);
    }

    @Override
    public String lookup(String key) {
        return bundle.getString(key);
    }


}
