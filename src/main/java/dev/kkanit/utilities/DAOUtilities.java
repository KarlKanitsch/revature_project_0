package dev.kkanit.utilities;

import dev.kkanit.dao.EmployeeDAO;
import dev.kkanit.dao.EmployeeDAOImpl;
import dev.kkanit.models.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

public class DAOUtilities {

    private static final String CONNECTION_USERNAME = System.getenv("DBUSERNAME");
    private static final String CONNECTION_PASSWORD = System.getenv("DBPASSWORD");
    private static final String URL = System.getenv("DBURL");
//    DBUSERNAME=Project0Admin;DBPASSWORD=project0password;DBURL=jdbc:postgresql://localhost:5432/Project0
//    private static final String CONNECTION_USERNAME = "Project0Admin";
//    private static final String CONNECTION_PASSWORD = "project0password";
//    private static final String URL = "jdbc:postgresql://localhost:5432/Project0";
    private static Connection connection;

    private static final Logger logger = LoggerFactory.getLogger(DAOUtilities.class);

    private DAOUtilities() {}

    public static synchronized Connection getConnection() throws SQLException {
//        System.out.println("USERNAME: " + CONNECTION_USERNAME);
//        System.out.println("URL: " + URL);
        if (connection!=null && connection.isClosed())
            logger.info("Previous connection closed. Opening a new connection to the RDBMS.");
        connection = DriverManager.getConnection(URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);
        return connection;
    }
}
