package dev.kkanit.dao;

import dev.kkanit.models.Employee;

import java.util.List;

public interface EmployeeDAO {

    List<Employee> getAllEmployees();
    Employee getEmployee(int employeeID);
    boolean addEmployee(String firstName, String lastName, double payRate, String federalID);
    boolean removeEmployee(int employeeID);
}
