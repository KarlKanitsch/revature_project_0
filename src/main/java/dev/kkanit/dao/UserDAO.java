package dev.kkanit.dao;

import dev.kkanit.models.UserAccount;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public interface UserDAO {

    List<String> getAllUsernames();
    List<String> getAllEmails();
    String getUserPassword(String username);
    Map<Integer, UserAccount> getAllUserAccounts();
    Map<Integer, UserAccount> getFilteredUserAccounts(String sql, List<Integer> ids);
    UserAccount getUserAccount(String username);
    boolean updateUserEmployeeID(int userID, int employeeID);
    int getPrivilege(String username);
    int getEmployeeID(String username);
    boolean addNewUser(String username, String password, String email);
    boolean changeUserPassword(String username, String password);
}
