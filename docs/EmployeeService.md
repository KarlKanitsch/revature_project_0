## Dependencies  
[SL4J Logger](http://www.slf4j.org/) 


## Fields  

|Access|Static|Final|DataType|Name|
|---|---|---|---|---|
|private|<center>F</center>|<center>F</center>|[EmployeeDAO](./docs/EmployeeDAO.md)|eDAO|
|private|<center>F</center>|<center>F</center>|[UserDAO](./docs/UserDAO.md)|uDAO|
|private|<center>F</center>|<center>F</center>|SL4J Logger|logger|  
  
## Methods  

|Access|Static|Signature|Return|Summary|  
|---|---|---|---|---| 
|public|<center>F</center>|getAllEmployees()|List<Employee>|Uses eDAO to retrieve all employee information from the database and returns it in a List|
|public|<center>F</center>|getEmployeeByUsername(String username)|Employee|Finds the employee's ID number using the username, then retrieves the employee records from the database and returns it in an Employee Object|
|pubic|<center>F</center>|getEmployeeIDByUsername|int|Finds the employee's ID number using the username and returns it|
|public|<center>f</center>|getEmployeeByID(int employeeID)|Employee|Retrieves employee information from the database for the supplied employeeID and returns a generated Employee Object|
|public|<center>F</center>|addNewEmployee(Map\<String, List\<String\>\> formMap)|boolean|Adds a new employee to the database with information obtained from the formMap|
|pubilc|<center>F</center>|deleteEmployeeRecord(Map\<String, List\<String\>\>) formMap|boolean|Parses the employee's ID number from the formMap and removes the employee from the database.|



[readme](../readme.md)